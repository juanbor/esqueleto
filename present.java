package present;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import java.awt.List;
import java.awt.Scrollbar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class prest extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					prest frame = new prest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public prest() {
		setTitle("BiblANIMA");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1137, 655);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 1121, 21);
		contentPane.add(menuBar);
		
		JMenu mnNewMenu = new JMenu("Usuario");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmAltaUsuario = new JMenuItem("Alta Usuario");
		
		mnNewMenu.add(mntmAltaUsuario);
		
		JMenuItem mntmBajaUsuario = new JMenuItem("Baja Usuario");
		
		mnNewMenu.add(mntmBajaUsuario);
		
		JMenu mnLibros = new JMenu("Libros");
		menuBar.add(mnLibros);
		
		JMenuItem mntmListarLibros = new JMenuItem("Listar Libros");
		mnLibros.add(mntmListarLibros);
		
		JMenuItem mntmAgregarLibro = new JMenuItem("Agregar Libro");
		mnLibros.add(mntmAgregarLibro);
		
		JMenuItem mntmBorrarLibro = new JMenuItem("Borrar Libro");
		mnLibros.add(mntmBorrarLibro);
		
		JMenu mnPrestamos = new JMenu("Prestamos");
		menuBar.add(mnPrestamos);
		
		JMenuItem mntmNuevoPrestamo = new JMenuItem("Nuevo Prestamo");
		
		mnPrestamos.add(mntmNuevoPrestamo);
		
		JMenuItem mntmBajaPrestamo = new JMenuItem("Baja Prestamo");
		mnPrestamos.add(mntmBajaPrestamo);
		
		JMenu mnAdministrador = new JMenu("Administrador");
		menuBar.add(mnAdministrador);
		
		JMenuItem mntmPersistirDatos = new JMenuItem("Persistir Datos");
		mnAdministrador.add(mntmPersistirDatos);
		
		JMenuItem mntmRecuperarDatos = new JMenuItem("Recuperar Datos");
		mnAdministrador.add(mntmRecuperarDatos);
		
		JMenuItem mntmAltaDatos = new JMenuItem("Alta Datos");
		mnAdministrador.add(mntmAltaDatos);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel_3.setBounds(150, 75, 820, 465);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		panel_3.setVisible(false);
		
		JLabel lblAltaPrestamo = new JLabel("Alta Prestamo");
		lblAltaPrestamo.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblAltaPrestamo.setBounds(342, 34, 135, 39);
		panel_3.add(lblAltaPrestamo);
		
		JButton button_2 = new JButton("Cancelar");
		button_2.setBounds(238, 387, 89, 23);
		panel_3.add(button_2);
		
		JButton button_3 = new JButton("Aceptar");
		button_3.setBounds(491, 387, 89, 23);
		panel_3.add(button_3);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(184, 136, 120, 20);
		panel_3.add(comboBox);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblUsuario.setBounds(203, 86, 73, 39);
		panel_3.add(lblUsuario);
		
		JLabel lblLibro = new JLabel("Libro");
		lblLibro.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblLibro.setBounds(526, 86, 73, 39);
		panel_3.add(lblLibro);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(507, 136, 120, 20);
		panel_3.add(comboBox_1);
		
		
		JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.setBounds(281, 255, 46, 20);
		panel_3.add(comboBox_2);
		
		JComboBox<String> comboBox_3 = new JComboBox<String>();
		comboBox_3.setBounds(363, 255, 82, 20);
		panel_3.add(comboBox_3);
		
		JComboBox<String> comboBox_4 = new JComboBox<String>();
		comboBox_4.setBounds(473, 255, 63, 20);
		panel_3.add(comboBox_4);
		
		
		
		JLabel lblDevolucion = new JLabel("Devolucion");
		lblDevolucion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDevolucion.setBounds(342, 194, 135, 39);
		panel_3.add(lblDevolucion);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel_2.setBounds(150, 75, 820, 465);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblBajaUsuario = new JLabel("Baja Usuario");
		lblBajaUsuario.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblBajaUsuario.setBounds(345, 28, 129, 39);
		panel_2.add(lblBajaUsuario);
		
		List list = new List();
		
		list.setBounds(320, 90, 180, 251);
		panel_2.add(list);
		
		Scrollbar scrollbar = new Scrollbar();
		scrollbar.setBounds(483, 90, 17, 251);
		panel_2.add(scrollbar);
		
		JButton btnAceptar = new JButton("Aceptar");
		
		btnAceptar.setBounds(493, 381, 89, 23);
		panel_2.add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panel_2.setVisible(false);
			}
		});
		btnCancelar.setBounds(240, 381, 89, 23);
		panel_2.add(btnCancelar);
		panel_2.setVisible(false);
		
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					if(list.getSelectedIndex() == -1) {
				        JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario", "Error",JOptionPane.INFORMATION_MESSAGE);
					}
					
					//ELIMINAR
				}
			});
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel_1.setBounds(150, 75, 820, 465);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setVisible(false);
		
		JLabel lblAltaUsuario = new JLabel("Alta Usuario");
		lblAltaUsuario.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblAltaUsuario.setBounds(351, 27, 117, 39);
		panel_1.add(lblAltaUsuario);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(414, 206, 116, 20);
		panel_1.add(passwordField_1);
		
		JLabel label = new JLabel("Nombre");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(269, 94, 110, 27);
		panel_1.add(label);
		
		JLabel label_1 = new JLabel("Apellido");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_1.setBounds(269, 129, 110, 27);
		panel_1.add(label_1);
		panel_1.setVisible(false);
		
		JLabel label_2 = new JLabel("Mail");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_2.setBounds(269, 164, 110, 27);
		panel_1.add(label_2);
		
		JLabel label_3 = new JLabel("Password");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_3.setBounds(269, 201, 110, 27);
		panel_1.add(label_3);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(414, 169, 116, 20);
		panel_1.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(414, 134, 116, 20);
		panel_1.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(414, 99, 116, 20);
		panel_1.add(textField_5);
		
		JButton button = new JButton("Aceptar");
		button.setBounds(441, 387, 89, 23);
		panel_1.add(button);
		
		JButton button_1 = new JButton("Cancelar");
		button_1.setBounds(255, 387, 89, 23);
		panel_1.add(button_1);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Profesor");
		
		chckbxNewCheckBox.setBounds(255, 280, 97, 23);
		panel_1.add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Estudiante");
		chckbxNewCheckBox_1.setBounds(357, 280, 97, 23);
		panel_1.add(chckbxNewCheckBox_1);
		
		JCheckBox chckbxNewCheckBox_2 = new JCheckBox("Bibliotecario");
		chckbxNewCheckBox_2.setBounds(456, 280, 97, 23);
		panel_1.add(chckbxNewCheckBox_2);
		
		JLabel lblSeleccioneUno = new JLabel("Seleccione uno:");
		lblSeleccioneUno.setBounds(255, 259, 97, 14);
		panel_1.add(lblSeleccioneUno);
		
		JLabel lblNewLabel = new JLabel("Orientacion: ");
		lblNewLabel.setBounds(257, 326, 75, 14);
		panel_1.add(lblNewLabel);
		
		JCheckBox chckbxAdministracion = new JCheckBox("Administracion");
		chckbxAdministracion.setBounds(357, 322, 97, 23);
		panel_1.add(chckbxAdministracion);
		
		JCheckBox chckbxTic = new JCheckBox("TIC");
		chckbxTic.setBounds(456, 322, 97, 23);
		panel_1.add(chckbxTic);
		
		chckbxNewCheckBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				if(chckbxNewCheckBox.isSelected()) {
					lblNewLabel.setVisible(false);
					chckbxAdministracion.setVisible(false);
					chckbxTic.setVisible(false);
				}else {
					lblNewLabel.setVisible(true);
					chckbxAdministracion.setVisible(true);
					chckbxTic.setVisible(true);
				}
				chckbxNewCheckBox_1.setSelected(false);
				chckbxNewCheckBox_2.setSelected(false);
				
				
			}
		});
		
		chckbxNewCheckBox_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				chckbxNewCheckBox.setSelected(false);
				chckbxNewCheckBox_2.setSelected(false);
				lblNewLabel.setVisible(false);
				chckbxAdministracion.setVisible(false);
				chckbxTic.setVisible(false);
			}
		});
		
		chckbxNewCheckBox_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				chckbxNewCheckBox_1.setSelected(false);
				chckbxNewCheckBox.setSelected(false);
				lblNewLabel.setVisible(false);
				chckbxAdministracion.setVisible(false);
				chckbxTic.setVisible(false);
			}
		});
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel.setBounds(346, 206, 428, 204);
		contentPane.add(panel);
		panel.setLayout(null);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(239, 63, 116, 20);
		panel.add(textField_2);
		
		JLabel lblIniciarSesion = new JLabel("Iniciar Sesion");
		lblIniciarSesion.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblIniciarSesion.setBounds(149, 11, 130, 27);
		panel.add(lblIniciarSesion);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMail.setBounds(94, 58, 110, 27);
		panel.add(lblMail);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPassword.setBounds(94, 95, 110, 27);
		panel.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(239, 100, 116, 20);
		panel.add(passwordField);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setBounds(266, 155, 89, 23);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancelar");
		btnNewButton_1.setBounds(79, 155, 89, 23);
		panel.add(btnNewButton_1);
		
		mntmBajaUsuario.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				
				panel.setVisible(false);
				panel_2.setVisible(true);
				
				//ACA CARGAN LOS DATOS
				list.add("Ejemplo 1");
				list.add("Ejemplo 2");
				
				//btnAceptar.setEnabled(false);
			
			
			}
		});
		
		mntmNuevoPrestamo.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				comboBox_2.addItem("1");
				comboBox_2.addItem("2");
				comboBox_2.addItem("3");
				comboBox_2.addItem("4");
				comboBox_2.addItem("5");
				comboBox_2.addItem("6");
				comboBox_2.addItem("7");
				comboBox_2.addItem("8");
				comboBox_2.addItem("9");
				comboBox_2.addItem("10");
				comboBox_2.addItem("11");
				comboBox_2.addItem("12");
				comboBox_2.addItem("13");
				comboBox_2.addItem("14");
				comboBox_2.addItem("15");
				comboBox_2.addItem("16");
				comboBox_2.addItem("17");
				comboBox_2.addItem("18");
				comboBox_2.addItem("19");
				comboBox_2.addItem("20");
				comboBox_2.addItem("21");
				comboBox_2.addItem("22");
				comboBox_2.addItem("23");
				comboBox_2.addItem("24");
				comboBox_2.addItem("25");
				comboBox_2.addItem("26");
				comboBox_2.addItem("27");
				comboBox_2.addItem("28");
				comboBox_2.addItem("29");
				comboBox_2.addItem("30");
				comboBox_2.addItem("31");
				
				comboBox_3.addItem("Enero");
				comboBox_3.addItem("Febrero");
				comboBox_3.addItem("Marzo");
				comboBox_3.addItem("Abril");
				comboBox_3.addItem("Mayo");
				comboBox_3.addItem("Junio");
				comboBox_3.addItem("Julio");
				comboBox_3.addItem("Agosto");
				comboBox_3.addItem("Septiembre");
				comboBox_3.addItem("Octubre");
				comboBox_3.addItem("Noviembre");
				comboBox_3.addItem("Diciembre");
				
				comboBox_4.addItem("2018");
				comboBox_4.addItem("2019");
				
				panel_3.setVisible(true);
				panel.setVisible(false); //borrar
				
			}
		});
		
		mntmAltaUsuario.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				panel.setVisible(false);
				panel_1.setVisible(true);
				
				lblNewLabel.setVisible(false);
				chckbxAdministracion.setVisible(false);
				chckbxTic.setVisible(false);
			}
		});
	}
}
